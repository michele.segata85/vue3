module.exports = {
    runtimeCompiler: true,
    publicPath: "/",
    outputDir: "dist",
    assetsDir: "static",
    indexPath: "index.html",
    css: { extract: false },  

  };
  